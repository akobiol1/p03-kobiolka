//
//  main.m
//  NewDoodleJump
//
//  Created by Amanda Kobiolka on 2/26/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
