//
//  ViewController.h
//  NewDoodleJump
//
//  Created by Amanda Kobiolka on 2/26/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>

NSMutableArray  *platformArray;
int jumpValue;
int xValue;
int yValue;
int leftCount;
int rightCount;
int numCarrots;

@interface ViewController : UIViewController {

    IBOutlet UILabel *carrotLabel;
    IBOutlet UIImageView *carrot;
    
    IBOutlet UIImageView *platform1;
    IBOutlet UIImageView *platform2;
    IBOutlet UIImageView *platform3;
    IBOutlet UIImageView *platform4;
    IBOutlet UIImageView *platform5;
    IBOutlet UIImageView *platform6;
    IBOutlet UIImageView *platform7;
    IBOutlet UIImageView *platform8;
    IBOutlet UIImageView *platform9;
    IBOutlet UIImageView *character;
    IBOutlet UIButton *moveLeft;
    IBOutlet UIButton *moveRight;
    NSTimer *leftTimer;
    NSTimer *rightTimer;
    NSTimer *fallTimer;
    
}
- (IBAction)goLeft:(id)sender;
- (IBAction)goRight:(id)sender;
-(void) moveLeft;
-(void) moveRight;
@end

