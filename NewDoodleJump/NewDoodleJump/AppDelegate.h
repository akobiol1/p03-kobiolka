//
//  AppDelegate.h
//  NewDoodleJump
//
//  Created by Amanda Kobiolka on 2/26/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

