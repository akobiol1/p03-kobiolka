//
//  ViewController.m
//  NewDoodleJump
//
//  Created by Amanda Kobiolka on 2/26/17.
//  Copyright © 2017 Amanda Kobiolka. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()



@end

@implementation ViewController

-(void)moveLeft{
    NSLog(@"IN MOVE left");

    
    jumpValue = jumpValue - 5;
    xValue = xValue - 5;
    character.center = CGPointMake(character.center.x + xValue, character.center.y-jumpValue);
    
    if ((character.center.x < 0) || (character.center.x > 310)|| (character.center.y > 530) ||(character.center.y < 0)) {
        NSLog(@"IN MOVE left, bounds check ");
        
        character.center = CGPointMake(141, 376);
        [leftTimer invalidate];
        xValue = 0;
        jumpValue = 0;
        fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.30 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];

    }
    //Got a carrot
    if(CGRectIntersectsRect(character.frame, carrot.frame)) {
        numCarrots++;
        NSString *string = [NSString stringWithFormat:@"Number of carrots: %d", numCarrots];
        carrotLabel.text = string;
        character.center = CGPointMake(141, 376);
        [leftTimer invalidate];
        xValue = 0;
        jumpValue = 0;
        fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.30 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];
        
    }
    

    
    for(int i = 0; i < 6; i++) {
        UIImageView *imageView = [platformArray objectAtIndex:i];
        
        if(CGRectIntersectsRect(character.frame, imageView.frame)) {
            NSLog(@"In Move left, intersectrect check");
            jumpValue = 0;
            int newY = character.center.y - 120;
            character.center = CGPointMake(character.center.x, newY);
            [leftTimer invalidate];
            xValue = 0;
            jumpValue = 0;
            fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.30 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];
            break;
            
        }
        
    }
}

-(void)moveRight{
     NSLog(@"IN MOVE right");

    
    jumpValue = jumpValue - 5;
    xValue = xValue + 5;
    character.center = CGPointMake(character.center.x + xValue, character.center.y-jumpValue);
    
    if ((character.center.x < 0) || (character.center.x > 310)|| (character.center.y > 530) ||(character.center.y < 0)) {
        NSLog(@"IN MOVE left, bounds check ");
        
        character.center = CGPointMake(141, 376);
        [rightTimer invalidate];
        xValue = 0;
        jumpValue = 0;
        fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.30 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];
        
    }
    //Got a carrot
    if(CGRectIntersectsRect(character.frame, carrot.frame)) {
        numCarrots++;
        NSString *string = [NSString stringWithFormat:@"Number of carrots: %d", numCarrots];
        carrotLabel.text = string;
        character.center = CGPointMake(141, 376);
        [leftTimer invalidate];
        xValue = 0;
        jumpValue = 0;
        fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.30 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];
    }
    
    for(int i = 0; i < 6; i++) {
        UIImageView *imageView = [platformArray objectAtIndex:i];
        
        if(CGRectIntersectsRect(character.frame, imageView.frame)) {
            NSLog(@"IN MOVE right, intersectrect check");
            jumpValue = 0;
            int newY = character.center.y - 120;
            character.center = CGPointMake(character.center.x, newY);
            [rightTimer invalidate];
            xValue = 0;
            jumpValue = 0;
            fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.30 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];
            break;
        }
        
    }
}

-(void)bouncing {
    NSLog(@"Bouncing");
    jumpValue = jumpValue - 4;
    character.center = CGPointMake(character.center.x, character.center.y-jumpValue);
    
    for(int i = 0; i < 6; i++) {
        UIImageView *imageView = [platformArray objectAtIndex:i];
        
        if(CGRectIntersectsRect(character.frame, imageView.frame)) {
            jumpValue = 0;
            int newY = character.center.y - 120;
            character.center = CGPointMake(character.center.x, newY);
        }
        
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"sky1.jpg"]]];
    
    platformArray = [[NSMutableArray alloc] init];
    [platformArray addObject:platform1];
    [platformArray addObject:platform2];
    [platformArray addObject:platform3];
    [platformArray addObject:platform4];
    [platformArray addObject:platform5];
    [platformArray addObject:platform6];
    
    fallTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(bouncing) userInfo:nil repeats:YES];


 

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)goLeft:(id)sender {
    [fallTimer invalidate];
    [rightTimer invalidate];
fallTimer:nil;
    rightTimer:nil;
    leftTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(moveLeft) userInfo:nil repeats:YES];
    NSLog(@"JumpLeft called");
    //jumpValue = jumpValue - 50;
}

- (IBAction)goRight:(id)sender {
    [fallTimer invalidate];
    [leftTimer invalidate];
    fallTimer:nil;
    leftTimer:nil;
    rightTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(moveRight) userInfo:nil repeats:YES];

    NSLog(@"JumpRight called");
    //jumpValue = jumpValue + 50;
}







@end
